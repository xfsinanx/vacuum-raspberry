import { ORIENTATION } from '../lib/constants';

export const moveTo = (side) => {
  switch (side) {
    case ORIENTATION.LEFT.value:
      console.log('Someone told me to move LEFT');
      activateLed(ORIENTATION.LEFT.led)
      break;
    case ORIENTATION.RIGHT.value:
      console.log('Someone told me to move RIGHT');
      activateLed(ORIENTATION.RIGHT.led)
      break;
    case ORIENTATION.TOP.value:
      console.log('Someone told me to move TOP');
      activateLed(ORIENTATION.TOP.led);
      break;
    case ORIENTATION.BOTTOM.value:
      console.log('Someone told me to move BOTTOM');
      activateLed(ORIENTATION.BOTTOM.led);
      break;
    case ORIENTATION.STOP.value:
      console.log('Someone told me to STOP');
      deactivateLeds();
      break;
    default:
      deactivateLeds();
      console.log('DEFAULT IT IS');
  }
};

const deactivateLeds = () => {
  if(ORIENTATION.LEFT.led.readSync() === 1)
    ORIENTATION.LEFT.led.writeSync(0)
  if(ORIENTATION.RIGHT.led.readSync() === 1)
    ORIENTATION.RIGHT.led.writeSync(0)
  if(ORIENTATION.TOP.led.readSync() === 1)
    ORIENTATION.TOP.led.writeSync(0)
  if(ORIENTATION.BOTTOM.led.readSync() === 1)
    ORIENTATION.BOTTOM.led.writeSync(0)
}

const activateLed = ( led ) => {
  if(led.readSync() != 1)
    led.writeSync(1);
}
