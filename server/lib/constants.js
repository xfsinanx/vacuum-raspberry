const Gpio = require('onoff').Gpio;

export const ORIENTATION = {
  LEFT: {
    value: 'left',
    led: new Gpio(4, 'out'),
  },
  RIGHT: {
    value: 'right',
    led: new Gpio(17, 'out'),
  },
  TOP: {
    value: 'top',
    led: new Gpio(27, 'out'),
  },
  BOTTOM: {
    value: 'bottom',
    led: new Gpio(22, 'out'),
  },
  STOP: {
    value: 'stop',
  },
};
