import Primus from './primuslib';
import config from './config';
import { moveTo } from '../actions/move';

class PrimusServiceBase {
  connectPrimus () {
    if (!this.primus) {
      try {
        this.primus.destroy();
      } catch (err) {
        console.warn('error destroying primus', err);//eslint-disable-line
      }
    }
    const endpoint = config.VACUUM.apiUrl;
    console.log(endpoint)//eslint-disable-line
    this.me = { _id: 5 };

    this.primus = new Primus(endpoint, {
      reconnect: {
        max: 20000,
        min: 500,
        retries: 50,
        'reconnect timeout': 10000,
      },
    });
    // this.subscribed = []
    this.handleEvents();
  }

  handleEvents () {
    this.primus.on('open', () => {
      if (this.reconnect) {
        clearInterval(this.reconnect);
      }
      // this.primus.emit('test:subscribe', { userId: this.me._id });
      //
      // // Send request to join the news room
      // this.primus.write({ action: 'join', room: 'news' });
      //
      // // Send request to leave the news room
      // this.primus.write({ action: 'leave', room: 'news' });
      //
      // this.primus.on('test:response', (data) => {
      //   moveTo(data.orientation);
      // });
      //
      // this.primus.on('data', (message) => {
      //   console.log(message);//eslint-disable-line
      // });
      this.primus.emit('join:vacuum', { userId: 5 });

      this.primus.on('vacuum:go', ({ orientation }) => {
        moveTo(orientation);
      });
      this.primus.on('vacuum:stop', () => {
        moveTo('stop');
      });
    });
  }
}

const PrimusService = new PrimusServiceBase();

export default PrimusService;
